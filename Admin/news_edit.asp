﻿
<!--#include file="top.asp"-->
<!--#include file="../fckeditor/fckeditor.asp" -->

<%
id=request.QueryString("id")
news_class_id=0
news_title=""
news_title_e=""
news_ahome="Internet"
news_ahome_e="Internet"
news_author="admin"
news_author_e="admin"
news_content=""
news_content_e=""
news_keyword=""
news_keyword_e=""
news_count=0
action="add"
if chkrequest(id) then
	sql="select * from news where news_id=" & id
	set rs=server.CreateObject("adodb.recordset")
	rs.open sql,conn,1,1
	if not rs.eof then
		news_class_id=rs("news_class_id")
		news_title=rs("news_title")
		news_title_e=rs("news_title_e")
		news_ahome=rs("news_ahome")
		news_ahome_e=rs("news_ahome_e")
		news_author=rs("news_author")
		news_author_e=rs("news_author_e")
		news_content=rs("news_content")
		news_content_e=rs("news_content_e")
		news_keyword=rs("news_keyword")
		news_keyword_e=rs("news_keyword_e")
		news_count=rs("news_count")
		action="edit"
	end if
	rs.close
	set rs=nothing
end if

%>
<HTML><HEAD><TITLE>管理中心</TITLE>
<META http-equiv=Content-Type content="text/html; charset=utf-8">
<LINK href="inc/djcss.css" type=text/css rel=StyleSheet>
<script language="javascript" src="../js/validator.js"></script>
</HEAD>
<body background=inc/dj_bg.gif>

<br>  
<table align="center" width="98%" border="1" cellspacing="0" cellpadding="4" bordercolor="#C0C0C0" style="border-collapse: collapse">
  <form name="form1" method="post" action="news_save.asp" onSubmit="return Validator.Validate(this,2);">
    <tr> 
      <td bgcolor="#E8E8E8"><a name="newnews">新的信息</a></font></td>
    </tr>
    <tr> 
      <td bgcolor="#FFFFFF" class="chinese">中文标题:
<input type="text" name="news_title" class="textarea" size="50" value="<%=news_title%>"  dataType="LimitB" min="2" max="200" msg="标题必须填写（2-200字符）">
*
         </td>
    </tr>
    
    <tr> 
      <td bgcolor="#FFFFFF" class="chinese">英文标题:
<input type="text" name="news_title_e" class="textarea" size="50" value="<%=news_title_e%>"  dataType="LimitB" min="2" max="200" msg="标题必须填写（2-200字符）">
*
         </td>
    </tr>
    <tr> 
      <td bgcolor="#FFFFFF" class="chinese">中文关键字:
        <input type="text" name="news_keyword" size="80" value="<%=news_keyword%>" class="textarea">
（填写相关热门的关键字使在百度里面更容易找到）
      </td>
    </tr>
    <tr> 
      <td bgcolor="#FFFFFF" class="chinese">英文关键字:
        <input type="text" name="news_keyword_e" size="80" value="<%=news_keyword_e%>" class="textarea">
      </td>
    </tr>
    <tr> 
      <td bgcolor="#FFFFFF" class="chinese">作 &nbsp;者:
<input name="news_author" type="text" class="textarea" value="<%=news_author%>" size="20">       
      </td>
    </tr>
    <tr> 
      <td bgcolor="#FFFFFF" class="chinese">英文作者:
<input name="news_author_e" type="text" class="textarea" value="<%=news_author_e%>" size="20">       
      </td>
    </tr>
    <tr> 
      <td bgcolor="#FFFFFF" class="chinese">来 &nbsp;源:
<input type="text" name="news_ahome" class="textarea" value="<%=news_ahome%>" size="50">
      </td>
    </tr>
    <tr> 
      <td bgcolor="#FFFFFF" class="chinese">英文来源:
<input type="text" name="news_ahome_e" class="textarea" value="<%=news_ahome_e%>" size="50">
      </td>
    </tr>
    <tr>
      <td height="31" bgcolor="#FFFFFF" class="chinese">选择分类: 
         <select name="news_class_id" id="news_class_id" dataType="Required" msg="没有选择信息分类">         
        <%
      '###############取出类别分类#################
      dim sql_news,rs_news
      sql_news="select news_id,news_type from news_class"
      set rs_news=server.createobject("adodb.recordset")
      rs_news.open sql_news,conn,1,1
      if Not(rs_news.BOF and rs_news.EOF) then 
     	Do While Not rs_news.EOF 
        %>
        <option value="<%= rs_news("news_id") %>" <%if clnG(rs_news("news_id"))=news_class_id then response.Write("selected")%>><%=rs_news("news_type") %></option>
        <% 
        rs_news.movenext
        loop       
      end if
	  rs_news.close
      set rs_news=Nothing  
      '####################取出分类结束#####################  
        %>
        </select>
         *(请选择正确的信息分类。) </td>
    </tr>
    <tr>
    <td colspan="2">中文内容</td></tr>
    <tr> 
      <td bgcolor="#FFFFFF" class="chinese">
      <%
	  
Set oFCKeditor = New FCKeditor
oFCKeditor.width="98%"
oFCKeditor.height="300"
oFCKeditor.value=replace_t(news_content)
oFCKeditor.Create "news_content"
%>
<tr>
    <td colspan="2">英文内容</td></tr>
    <tr> 
      <td bgcolor="#FFFFFF" class="chinese">
<%
Set oFCKeditor = New FCKeditor
oFCKeditor.width="98%"
oFCKeditor.height="300"
oFCKeditor.value=replace_t(news_content_e)
oFCKeditor.Create "news_content_e"

	  %>
       
       　            </td>
    </tr>
    <tr> 
      <td bgcolor="#F5F5F5" class="chinese" height="30" align="center">
        <input type="submit" name="Submit2" value="确定提交" class="button">
        <input type="reset" name="Reset" value="清空重填" class="button">
      </td>
    </tr>
    <input type="hidden" name="action" value="<%=action%>">
   <input type="hidden" name="id" value="<%=id%>">
  </form>
</table>

<%

closeconn
%>