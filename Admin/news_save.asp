﻿<!--#include file="top.asp"-->
<%
dim founderr,errmsg
dim author,ahome,keyword,title,news_class_id,content,news_language
founderr=false
errmsg=""
i=0
action=request("action")
language=request.QueryString("language")
page=request.QueryString("page")
keyword=request.QueryString("keyword")
cid=request.QueryString("cid")
id=request("id")
if action="add" or action="edit" then
	author=checkstr(request.form("news_author"))
	author_e=checkstr(request.form("news_author_e"))
	ahome=checkstr(request.form("news_ahome"))
	ahome_e=checkstr(request.form("news_ahome_e"))
	keyword=checkstr(request.form("news_keyword"))
	keyword_e=checkstr(request.form("news_keyword_e"))
	title=checkstr(request.form("news_title"))
	title_e=checkstr(request.form("news_title_e"))
	news_class_id=checkstr(request.form("news_class_id"))
	content=request.form("news_content")	
	content_e=request.form("news_content_e")
	if not chkRange(title,2,50) then
		founderr=true
		i=i+1
		errmsg=errmsg&i&"),你必须输入信息的中文标题！"	
	End if
	if not chkRange(title_e,2,255) then
		founderr=true
		i=i+1
		errmsg=errmsg&i&"),你必须输入信息的英文标题！"	
	End if
	if not chkrequest(news_class_id) then
		founderr=true
		i=i+1
		errmsg=errmsg&i&"),你必须选择信息的分类！"	
	else
		sql="select news_id from news_class where news_id="&news_class_id
		set rs=server.createobject("adodb.recordset")
		rs.open sql,conn,1,1
		if rs.bof and rs.eof then
			founderr=true
			i=i+1
			errmsg=errmsg&i&"),你选择信息分类并不存在！"
		else
			'news_language=rs("news_language")
		end if	
		rs.close
		set rs=nothing
	End if
	if chknull(content,10) then
		founderr=true
		i=i+1
		errmsg=errmsg&i&"),你必须输入中文信息的内容（10字符以上）！"	
	End if
	if chknull(content_e,10) then
		founderr=true
		i=i+1
		errmsg=errmsg&i&"),你必须输入英文信息的内容（10字符以上）！"	
	End if
end if

if action="edit" or action="del" then
	if not chkrequest(id) then alert "error","",1
end if

if founderr then alert errmsg,"",1
if not chkrequest(news_language) then news_language=0 else news_language=cint(news_language)
if action="add" then
	sql="select * from news"
	set rs=server.createobject("adodb.recordset")
	rs.open sql,conn,1,3
	rs.addnew
	rs("news_author")=author
	rs("news_author_e")=author_e
	rs("news_ahome")=ahome
	rs("news_ahome_e")=ahome_e
	rs("news_keyword")=keyword
	rs("news_keyword_e")=keyword_e
	rs("news_title")=title
	rs("news_title_e")=title_e
	rs("news_class_id")=news_class_id
	'rs("news_language")=news_language
	rs("news_content")=replace_text(content)
	rs("news_content_e")=replace_text(content_e)
	rs("news_date")=date
	rs.update
	id=rs("news_id")
	rs.close
	set rs=Nothing
End if

if action="edit" then
sql="select * from news where news_id="&id
set rs=server.createobject("adodb.recordset")
rs.open sql,conn,1,3
	rs("news_author")=author
	rs("news_author_e")=author_e
	rs("news_ahome")=ahome
	rs("news_ahome_e")=ahome_e
	rs("news_keyword")=keyword
	rs("news_keyword_e")=keyword_e
	rs("news_title")=title
	rs("news_title_e")=title_e
	rs("news_class_id")=news_class_id
	'rs("news_language")=news_language
	rs("news_content")=replace_text(content)
	rs("news_content_e")=replace_text(content_e)	
	rs.update
	rs.close
	set rs=Nothing
End if



if action="del" then
	sql="select * from news where news_id="&id
	set rs=server.createobject("adodb.recordset")
	rs.open sql,conn,1,3
	rs.delete
	rs.close
	set rs=Nothing 	
End if
closeconn
response.Redirect("news.asp?language="&language&"&page="&page&"&cid="&cid&"&keyword="&server.URLEncode(keyword))
%>