﻿<%
'snum条数 swhere附件条件 ssort排序 slist是否分页 slanguage中英文
function getnews(snum,swhere,slist,slanguage)
dim trs,i
dim tt,turl,tturl,tdate,tcontent
dim tpage
if slist=0 then
sql="select top "&snum&" news_id,news_title,news_title_e,news_date,news_count from news where news_id>0"&swhere&" order by news_id desc "
else
	tpage=request.QueryString("page")
	if chkrequest(tpage) then tpage=cint(tpage) else tpage=0
	if tpage>1 then
		sql="SELECT TOP "&snum&" news_id,news_title,news_title_e,news_date,news_count from news where (news_id <(SELECT MIN(news_id) FROM (SELECT TOP "&((tpage-1)*snum)&" news_id FROM news where news_id>0"&sWhere&" order by news_id desc) AS tblTMP)) and news_id>0"&sWhere&" order by news_id desc"
	else
	sql="select top "&snum&" news_id,news_title,news_title_e,news_date,news_count from news where news_id>0"&swhere&" order by news_id desc"
	end if
end if
'response.Write(sql)
'response.End()
set trs=server.CreateObject("adodb.recordset")
trs.open sql,conn,1,1
if not (trs.eof and trs.bof) then
	i=0
	do while not trs.eof and i<snum
		tid=trs("news_id")
		if slanguage=0 then
		tt=trs("news_title")
		turl="/news_view.asp?id="&tid
		else
		tt=trs("news_title_e")
		turl="/ennews_view.asp?id="&tid
		end if
		tdate=year(trs("news_date")) & "-" & month(trs("news_date")) & "-" & day(trs("news_date"))
		'if datediff("d",tdate,now())<30 then hot="<img src="""&citymodepath&"/images/hot.gif"" border=""0"" />" else hot=""
		if slist=0  then
			if 	slanguage=0 then	
			tlen=12
			else
			tlen=24
			end if
		else
		tlen=100
		end if
		
		showdate="<span>"&tdate&" </span>" 
		if slist=0 then
		tcontent=tcontent&"<li><a href="""&turl&""" title="""&tt&""">"&left(tt,tlen)&"</a>"&showdate&"</li>" & vbcrlf 	
		else
		tcontent=tcontent&"<li>"&showdate&"<a href="""&turl&""" title="""&tt&""">"&left(tt,tlen)&"</a></li>" & vbcrlf 
		end if	
		trs.movenext
		i=i+1
	loop
else
	tcontent="No Content"
end if
trs.close
set trs=nothing
getnews=tcontent
tcontent=""
end function


function get_left_class(bid,moren_id,slanguage,smode,spage)
dim trs,tcontent,tid,tt
set trs=server.CreateObject("adodb.recordset")
tid=request.QueryString("id")
if not chkrequest(tid) then tid=moren_id else tid=clng(tid)
sql="select * from inc_class where i_type_id=" & bid

trs.open sql,conn,1,1
if not trs.eof then
	do while not trs.eof
		if slanguage=0 then
			tt=trs("i_type")
		else
			tt=trs("i_type_e")
		end if
		if smode=0 then
			tcontent=tcontent & "<a href=""" & spage & "?id=" & trs("i_id") & """>" & tt & "</a>" & vbcrlf
		else
			if clng(trs("i_id"))=tid then
				tcontent=tcontent & "<li class=""on""><a href=""" & spage & "?id=" & trs("i_id") & """>" & tt & "</a></li>" & vbcrlf
			else	
				tcontent=tcontent & "<li><a href=""" & spage & "?id=" & trs("i_id") & """ >" & tt & "</a></li>" & vbcrlf
			end if
		end if
		trs.movenext
	loop
end if
trs.close
set trs=nothing'
get_left_class=tcontent
tcontent=""
end function

function get_left_news_class(moren_id,slanguage,smode)
dim trs,tcontent,tid,tt,spage
set trs=server.CreateObject("adodb.recordset")
tid=request.QueryString("id")
if slanguage=0 then spage="news.asp" else spage="ennews.asp"
if not chkrequest(tid) then tid=moren_id else tid=clng(tid)
sql="select * from news_class"

trs.open sql,conn,1,1
if not trs.eof then
	do while not trs.eof
		if slanguage=0 then
			tt=trs("news_type")
		else
			tt=trs("news_type_e")
		end if
		if smode=0 then
			tcontent=tcontent & "<a href=""" & spage & "?id=" & trs("news_id") & """>" & tt & "</a>" & vbcrlf
		else
			if clng(trs("news_id"))=tid then
				tcontent=tcontent & "<li class=""on""><a href=""" & spage & "?id=" & trs("news_id") & """>" & tt & "</a></li>" & vbcrlf
			else	
				tcontent=tcontent & "<li><a href=""" & spage & "?id=" & trs("news_id") & """ >" & tt & "</a></li>" & vbcrlf
			end if
		end if
		trs.movenext
	loop
end if
trs.close
set trs=nothing'
get_left_news_class=tcontent
tcontent=""
end function

function get_type_name(id,slanguage)
dim trs
set trs=server.CreateObject("adodb.recordset")
sql="select * from inc_type where id=" & id
trs.open sql,conn,1,1
if trs.eof then
	get_type_name=""
else
	if slanguage=0 then
	get_type_name=trs("inc_name")
	else
	get_type_name=trs("inc_name_e")
	end if
end if
trs.close
set trs=nothing
end function



'返回数组 0为失败 1为成功
function get_class_name(byval id)
dim trs,i_type,i_type_name,i_type_e,i_type_name_e
set trs=server.CreateObject("adodb.recordset")
sql="select * from inc_class where i_id=" & id
trs.open sql,conn,1,1
if trs.eof then
	get_class_name=array(1,"")
else
	i_type=trs("i_type")
	i_type_e=trs("i_type_e")
	i_type_name=trs("i_type_name")
	i_type_name_e=trs("i_type_name_e")
	get_class_name=array(0,i_type,i_type_name,i_type_e,i_type_name_e)
end if
trs.close
set trs=nothing
end function


function getAbout(sid,slanguage)
dim trs
sql="select top 1 inc_text,inc_text_e from inc_info where inc_class_id="&sid
set trs=server.CreateObject("adodb.recordset")
trs.open sql,conn,1,1
if not (trs.bof and trs.eof) then
	if slanguage=0 then
		getAbout=replace_t(trs("inc_text"))
	else
		getAbout=replace_t(trs("inc_text_e"))
	end if
else
getAbout="No Content"
end if
trs.close
set trs=nothing
end function


function getHomeAbout(sid,slen,slanguage)
dim trs
sql="select top 1 inc_text,inc_text_e from inc_info where inc_class_id="&sid
set trs=server.CreateObject("adodb.recordset")
trs.open sql,conn,1,1
if not (trs.bof and trs.eof) then
	if slanguage=0 then
		getHomeAbout=leftt(trs("inc_text"),slen)
	else
		getHomeAbout=leftt(trs("inc_text_e"),slen)
	end if
else
getHomeAbout="No Content"
end if
trs.close
set trs=nothing
end function

%>
