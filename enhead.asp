﻿
<meta name="Copyright" content="Copyright (c) <%=year(now())%> <%=cityurl%>" /> 
<link href="<%=skinpath%>/css/css.css" rel="stylesheet" type="text/css" />
<meta name="Keywords" content="<%=kw%>" /> 
<meta name="Description" content="<%=desc%>" /> 
<script src="/jquery/jquery.js" type="text/javascript"></script>
<script src="/js/function.js" type="text/javascript"></script>
<script src="/js/dropdowntabs.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%=skinpath%>/css/slidingdoors.css" />
<title><%=pt%></title>


</head>
<body>
<div id="container">
<div id="top">
    <div class="topleft"></div>
    <div class="topright">
    	<div class="topright_top"><a href="#" onClick="cjx.setHome(this,'<%=cityurl%>');">Set Home</a> | <a href="/">CHINESE</a> | <a href="javascript:;" onClick="cjx.myAddBookmark('<%=cityname%>','<%=cityurl%>');">Add to Favorites</a></div>
        <div class="lanmu" id="slidemenu">
        <ul>
        	<li><a href="default.asp" <%=lanmu(0)%>><span class="t">Home</span><span>首页</span></a></li>
            <li><a href="enabout.asp" <%=lanmu(1)%> rel="dropmenu_about"><span class="t">About us</span><span>关于我们</span></a></li>
            <li><a href="enservices.asp" <%=lanmu(2)%> rel="dropmenu_services"><span class="t">Services</span><span>服务范畴</span></a></li>
            <li><a href="enbisness.asp" <%=lanmu(3)%> rel="dropmenu_bisness"><span class="t">E-Bisness</span><span>电子商务</span></a></li>
            <li><a href="ennews.asp" <%=lanmu(4)%> rel="dropmenu_new"><span class="t">News</span><span>新闻中心</span></a></li>
            <li><a href="encontact.asp" <%=lanmu(5)%> rel="dropmenu_contact"><span class="t">Contact Us</span><span>联系我们</span></a></li>            
        </ul>
        </div>
         <!--start-->
         <br style="clear: left;" />
<br class="IEonlybr" />



<!--1st drop down menu -->                                                   
<div id="dropmenu_about" class="dropmenudiv_c" style="width: 120px;">
<%=get_left_class(1,0,1,0,"enabout.asp")%>
</div>


<!--2nd drop down menu -->                                                
<div id="dropmenu_services" class="dropmenudiv_c" style="width: 120px;">
<%=get_left_class(2,0,1,0,"enservices.asp")%>
</div>

<div id="dropmenu_bisness" class="dropmenudiv_c" style="width: 120px;">
<a href="enbisness.asp" >Freight inquiry</a>
<a href="endownload.asp">Download</a>
</div>

<div id="dropmenu_new" class="dropmenudiv_c" style="width: 120px;">
<%=get_left_news_class(1,1,0)%>
</div>

<div id="dropmenu_contact" class="dropmenudiv_c" style="width: 120px;">
<a href="encontact.asp" >Contact Us</a>
<a href="enabout.asp?id=25">Organizations</a>
<a href="enabout.asp?id=34">Partners</a>
<a href="enabout.asp?id=35">Overseas Agents</a>
<a href="enabout.asp?id=37">Recruitment</a>
</div>

<script type="text/javascript">
//SYNTAX: tabdropdown.init("menu_id", [integer OR "auto"])
tabdropdown.init("slidemenu")
</script>
        <!--end-->
    </div>
</div>
<div class="clear"></div>


<div id="mainbanner">

<%
if is_home=0 then
	w=837
	h=433
%>
<img src="<%=skinpath%>/images/banner<%=is_home%>_e.gif" />

<%
else
	w=804
	h=184
%>
<img src="<%=skinpath%>/images/banner<%=is_home%>_e.gif" />
<%
end if

%><!----></div>